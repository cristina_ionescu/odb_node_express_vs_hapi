var https = require('http');
var config = require("../../config");
var couchbase = require('couchbase');
var N1qlQuery = require('couchbase').N1qlQuery;
var myCluster = new couchbase.Cluster('couchbase://' + config.couchbase.server);
var myBucket = myCluster.openBucket(config.couchbase.bucket);
var bucket = config.couchbase.bucket;


module.exports = function(){
	return {
		getDocument: function (req, res){
			var query = N1qlQuery.fromString('SELECT * FROM ' + bucket +';');

			myBucket.query(query, function(err, dbres) {
				if (err) {
			    	console.log('query failed', err);
			    	res.status(500).send({ message: 'Failed to get requested document.' });
			  	}
				res.send(dbres);
			});
		},

		getDocumentById: function (req, res){
			var docID = req.params.docID;
			myBucket.get(docID, function(err, dbres) {
				if (err) {
			    	console.log('query failed', err);
			    	res.status(500).send({ message: err.message });
			  	} else {	    
			    	res.send(dbres);
			  	}

			});
		},

		createDocument: function(req, res){
			var body = req.body;

			console.log("BODY " + JSON.stringify(body))

			if(body && body._id){
				myBucket.insert(body._id, body, function(err, dbres){
					if(err){
						console.log('query failed', err);
				    	res.status(500).send({ message: 'Failed to create requested document.' });
					}
					res.send("Document created successfully");
				});
			} else {
				// this error message is for testing purposes
				res.status(400).send({message: 'Bad Request. Missing id and doc field'});
			}

		},

		updateDocument: function(req, res){
			var body = req.body;
			myBucket.upsert(body._id, body, function(err, dbres){
				if(err){
					console.log('query failed', err);
			    	res.status(500).send({ message: 'Failed to create requested document.' });
				}
				res.send(dbres);
			});
		},

		updateDocumentNoOverwrite: function(req, res){
			var body = req.body;

			var query = N1qlQuery.fromString('UPDATE express USE KEYS "'+body._id+'"  SET fieldToUpdate =  "'+body.fieldToUpdate+'" ;');

			myBucket.query(query, function(err, dbres) {
				if (err) {
			    	console.log('query failed', err);
			    	res.status(500).send({ message: 'Failed to get requested document.' });
			  	}
				res.send(dbres);
			});
		}
	}
}();