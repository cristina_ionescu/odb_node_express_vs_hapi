var express = require('express');
var bodyParser = require('body-parser');
var couchbase = require("couchbase");
var config = require("../../config");

var app = express();

var handlers = require('./handlers');

app.exposeEndpoints = function(){
	app.get('/', function(req, res){
		res.send('Test GET Works');
	});

	app.get('/getdoc', handlers.getDocument);
	app.get('/getdoc/:docID', handlers.getDocumentById);
	app.post('/insertdoc', handlers.createDocument);
	app.put('/updatedoc', handlers.updateDocument);
	app.put('/updatedoc/nooverwrite', handlers.updateDocumentNoOverwrite);

}

exports.boot = function(config){
	app.locals.httpServer = config.httpServer;
	app.locals.appVersion = config.appVersion;
	app.locals.pretty = true;

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
 	app.exposeEndpoints();

 	app.listen(config.httpServer.port, function() {
        console.log('httpServer started at ' + config.httpServer.port);
    });

}