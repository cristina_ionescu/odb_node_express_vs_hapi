var express = require('./src/example/example.js');
var config = require('./config');
var pkg = require('./package');

config.appVersion = pkg.version;

express.boot(config);