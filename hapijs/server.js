'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Fs = require('fs');
const Routes = require('./routes');

const server = new Hapi.Server();
server.connection({ port: 3000 });
server.register(Inert, () => {});
server.route(Routes);


server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});
