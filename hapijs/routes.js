'use strict';

const Joi = require('joi');
var couchbase = require('couchbase');
var cluster = new couchbase.Cluster('couchbase://127.0.0.1');
var bucket = cluster.openBucket('default');
var N1qlQuery = require('couchbase').N1qlQuery;


// curl http://localhost:3000/search\?name\=Paul2
const getSearchConfig = {
    handler: function (request, reply) {
        var filt = request.query.name;
        console.log("Received GET search - :" + filt);
        var q = N1qlQuery.fromString("SELECT * FROM default WHERE name='"+filt+"'");
        bucket.query(q,function(err,result){
            if (err){
                console.log(filt + " NOT Found");
            }
            else{
                console.log("Result:",result);
            }
        });
        reply({ greeting: 'Searching ' + filt });
    },
    validate: {
        query: { name: Joi.string(2).min(1).required() } 
    }
};


// curl http://localhost:3000/exists\?id\=1047
const getExistsConfig = {
    handler: function (request, reply) {
        console.log("Received GET exists - jsonVersion:" + JSON.stringify(request.query));
        var testId = 'testId'+request.query.id;
        bucket.get(testId, function(err, result) {
            if (err){
                console.log(testId + " NOT Exists on the DataBase");
            }
            else{
                console.log(testId + " Exists on the DataBase");
            }
        });
        reply({ greeting: 'Exists? ' + testId});
    },
    validate: {
        query: { 
            id: Joi.number().min(100).max(999999999).required() 
        } 
    }
};


// curl http://localhost:3000/get\?name\=Paul2
const getHelloConfig = {
    handler: function (request, reply) {
        console.log("Received GET from " + request.query.name);
        reply({ greeting: 'hello ' + request.query.name });
    },
    validate: {
        query: { name: Joi.string(2).min(1).required() } 
    }
};


// curl http://localhost:3000/post -d name=Paul2
// curl http://localhost:3000/post -d "name=Paul2&id=1000"
const helloPostHandler = function(request, reply) {
    console.log("Received POST from " + request.payload.name + "; id=" + (request.payload.id || 'anon'));
    if (request.payload.id) {
        var jsonVersion = JSON.stringify(request.payload);
        console.log('jsonVersion: ', jsonVersion);
        var testId = 'testId'+request.payload.id;
        bucket.upsert(testId, jsonVersion, function (err, response){
            if (err) {
                console.log('Failed to save to Couchbase', err);
                return;
            } else {
                console.log('Saved to Couchbase!');
            }
        });
    }
    reply({ 
        greeting: 'POST hello to ' + request.payload.name
    });
}


// curl http://localhost:3000/update -d "name=Paul2&newname=NotMoreRandom&id=1005"
const updatePostHandler = function(request, reply) {
        var testId = 'testId' + request.payload.id;
         var jsonVersion = JSON.stringify(request.payload);
        console.log("Received Update POST from " + jsonVersion);
        bucket.get(testId, function(err, result) {
            if (err){
                console.log(testId + " NOT Exists on the DataBase");
            }
            else{
                console.log(testId + " Exists on the DataBase");
                var q = N1qlQuery.fromString('UPDATE default SET name ="' + request.payload.newname + '" WHERE id = ' + request.payload.id);
                bucket.query(q,function(err,result){            
                    if (err){
                        console.log("ERROR WHILE UPDATE: " + err + " -> " + q);
                    }
                    else{
                        console.log("Update OK");
                    }
                });
            }
        });
};

const postHelloConfig = {
    handler: helloPostHandler, 
    validate: { 
        payload: { 
            name: Joi.string().min(1).required(), 
            id: Joi.number().min(100).max(999999999),
            uploadFile: Joi.object().optional()
    } }
};


const postUpdateConfig = {
    handler: updatePostHandler,
    validate: { 
        payload: { 
            name: Joi.string().min(1),
            newname: Joi.string().min(1).required(),  
            id: Joi.number().min(100).max(999999999).required()
    } }
};

module.exports = [{
   		method: 'GET',
        path: '/search',
        config: getSearchConfig
    },
    {
        method: 'GET',
        path: '/exists',
        config: getExistsConfig
    },
    {
        method: 'GET',
        path: '/get',
        config: getHelloConfig
    },
    {
        method: 'POST',
        path: '/post',
        config: postHelloConfig
    },
    {
        method: 'POST',
        path: '/update',
        config: postUpdateConfig
    }
];